\ProvidesClass{uebungsblatt}
\LoadClass[a4paper,oneside]{article}
\ProcessOptions\relax

\RequirePackage[pass]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{german}
\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{amsxtra}
\RequirePackage{amsthm}
\RequirePackage{mathtools}
\RequirePackage{titling}
\RequirePackage{fancyhdr}
\RequirePackage{tikz}

\usetikzlibrary{matrix}
\usetikzlibrary{positioning}
\usetikzlibrary{circuits.ee.IEC}
\usetikzlibrary{circuits.logic.IEC}

\theoremstyle{definition}
\newtheorem{ex}{Aufgabe}

\renewcommand\theenumi{\alph{enumi}}
\renewcommand\labelenumi{(\theenumi)}

\pagestyle{plain}
\fancypagestyle{plain}{%
  \rhead{%
    Wintersemester 2016/17\\
    Brückenkurs Mathematik}
  \lhead{%
    Prof.\,Dr.\,Marc Nieper-Wißkirchen\\
    Caren Schinko, M.\,Sc.}
  \cfoot{\thepage}
  \renewcommand\headrulewidth{0pt}}

\pretitle{\begin{center}\Large\bfseries}
\posttitle{\par\end{center}}
\preauthor{}
\postauthor{}
\predate{}
\postdate{}
\setlength{\droptitle}{-40pt}

\date{}
